

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];


function register(newUser) {

    let checkUser = registeredUsers.includes(newUser);
   
        
    if (checkUser) {
       
        alert("Registration failed. Username already exists!");
    
    }

    else{
        
        registeredUsers.push(newUser);  
        alert("Thank you for registering!");
    
    }
}




function addFriend(username) {
    
    let findUser = registeredUsers.indexOf(username);

        
    if (findUser >= 0) {
        friendsList.push(registeredUsers[findUser]); 
        alert(`You have added ${registeredUsers[findUser]} as a friend!`);
    
    }

    else{
         
        alert("User not found.");
    }
    }




function displayFriends() {
    
    if (friendsList.length > 0) {
           
        friendsList.forEach(function(friend)
        {
            console.log(friend);
        }
        )
    }
    
    else{
        alert( "You currently have 0 friends. Add one first.");
    }
   

}




function displayNumberOfFriends() {

    if (friendsList.length > 0 ) {
       alert(`You currently have ${friendsList.length} friends.`);
    }
    else{
        alert(`You currently have 0 friends. Add one first.`);
    }
}



function deleteLastAddedFriend(index) {
    
    if (friendsList.length > 0 ) {
        friendsList.pop();
    }
    else{
        alert(`You currently have 0 friends. Add one first.`);
    }

}



function deleteAFriend(index) {
    
    if (friendsList.length > 0 ) {
        friendsList.splice(index,1);
    }
    else{
        alert(`You currently have 0 friends. Add one first.`);
    }

}

